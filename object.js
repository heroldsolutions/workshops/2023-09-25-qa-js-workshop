const invoiceJson = require("./invoiceBasicData.json");

const obj1 = {
    key1: 123,
    key2: 321,
    key3: {
        deep1: "value",
    },
};

console.log(obj1.key3.deep1);

const invoiceBasicData = {
    mandator: "Döhle",
    debitor: "DAK",
    address: "Somewhere in the world",
};

// invoiceBasicData = {}
invoiceBasicData.address = "somwhere else";
