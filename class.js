class Counter {
    _count = 0;

    getCount() {
        return this._count;
    }

    up() {
        this._count++;
    }

    down() {
        this._count--;
    }
}

const counter1 = new Counter();
const counter2 = new Counter();
counter1.up();
console.log(counter1.getCount());
console.log(counter2.getCount());
