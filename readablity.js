// bad readability improvement
// replacement of one easy to read line with something else
function multiplie(value1, value2) {
    return value1 * value2;
}

console.log(2 * 2);
console.log(multiplie(2, 2));
console.log(multiplie(3, 2));

// god example of readability improvement
// hide "complicated" code behind a readable function name
function average(value1, value2) {
    return (value1 + value2) / 2;
}

console.log(average(2, 5));
