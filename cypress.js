it("example", () => {
    cy.visit("/some/page/with/data");

    cy.findByTestid("input-random").then((element) => {
        const inputRandomValue = element.value;

        cy.visit("/data/grid/page");
        cy.containe(inputRandomValue).should("be.visible");
    });
});
