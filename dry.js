const dry = require("./util/dry.js");

let value = "";
for (let i = 0; i < 9; i++) {
    value += "my data " + i + "\n";
}
// let i = 0;
// while (i < 9) {
//     value += "my data " + i + "\n";
//     i++;
// }
// do {
//     value += "my data " + i + "\n";
//     i++;
// } while (i < 9);

// value += "my data 0\n";
// value += "my data 1\n";
// value += "my data 2\n";
// value += "my data 3\n";
// value += "my data 4\n";
// value += "my data 5\n";
// value += "my data 6\n";
// value += "my data 7\n";
// value += "my data 8\n";
console.log(value);

// let value2 = "";
// value += "My Title with data1\n";
// value += "--------------------\n";
// value += "my body is wonderfull\n";
// value += "--------------------\n";
const value2 = dry.dryMessage();
console.log(value2);
