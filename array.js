const arr1 = ["value123", "value321", "my value", "some value"];

// for (let i = 0; i < arr1.length; i++) {
//     console.log(arr1.at(i));
// }

for (const item of arr1) {
    console.log(item);
}

// invoice generator position example
// position.mock.ts
const positions = [
    {
        description: "first position",
        price: 123,
    },
    {
        description: "2nd position",
        price: 321,
    },
];

// invoicetest.cy.ts
// import { positions } from "./position.mock.ts"
for (const position of positions) {
    cy.page("inovoice").addPositon(position);
}
