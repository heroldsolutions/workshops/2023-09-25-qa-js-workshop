function myLog(value) {
    let scopedValue = 5;
    var value1 = 3 + 5;
    console.log(value, scopedValue);
}

console.log("test");
console.log("test 2");
myLog("my log function");

let value2 = 3 + 5;
const value3 = 3 + 5;

console.log(value2);
value2 = value2 + 2;
console.log(value2);

// console.log(scopedValue);
// console.log(value1);
