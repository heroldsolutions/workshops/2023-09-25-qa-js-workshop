class InvoicePage {
    fillBasicData(mandator, debitor, address) {
        cy.findByTestid("mandator").type(mandator);
        cy.findByTestid("debitor").type(debitor);
        cy.findByTestid("address").type(address);
    }

    addPosition(desciption, price) {
        cy.findByTestid("desciption").type(desciption);
        cy.findByTestid("price").type(price);
    }
}

it("invoice test", () => {
    cy.page("invoice").fillBasicData("döhle", "DAK", "somewhere in the world");
    cy.page("invoice").addPosition("Position 1", 123);
    cy.page("invoice").addPosition("Position 2", 321);
});
